using CheckoutKata.Interfaces;
using CheckoutKata.Models;
using CheckoutKata.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using static System.Math;

namespace CheckoutTest
{
    [TestFixture]
    public class CheckoutServiceTests
    {
        private ICheckout _sut;
        private Mock<IPriceService> _priceService;

        [SetUp]
        public void Setup()
        {
            _priceService = new Mock<IPriceService>(MockBehavior.Strict);
            var mockPrice = new List<Price>()
            {
                new Price() { ProductCode = "test1", UnitPrice = 0.50D},
                new Price() { ProductCode = "test2", UnitPrice = 0.70D, SpecialOfferCount = 2, SpecialOfferPrice = 1.00D },
            };
            _priceService.Setup(x => x.GetPrice()).Returns(mockPrice);
            _sut = new Checkout(_priceService.Object);
        }

        [Test]
        public void Scan_Without_Special_Offer_Test()
        {
            var item = new Item() { ProductCode = "test1", Count = 3 };
            _sut.Scan(item);
            var expectedAmount = _sut.GetItems().Where(x => x.ProductCode == item.ProductCode).Sum(x => x.UnitPrice * x.Count) ?? 0;
            Assert.AreEqual(_sut.GetItems().Count(x => x.ProductCode == item.ProductCode), 1);
            Assert.AreEqual(_sut.GetItems().Where(x => x.ProductCode == item.ProductCode).Sum(x => x.Count), item.Count);
            Assert.AreEqual(_sut.GetItems().Where(x => x.ProductCode == item.ProductCode).Sum(x => x.Amount), expectedAmount);
        }

        [Test]
        public void Scan_With_Special_Offer_Test()
        {
            var item1 = new Item() { ProductCode = "test2", Count = 2 };
            _sut.Scan(item1);
            var expectedAmount = Round(_sut.GetItems().Where(x => x.ProductCode == item1.ProductCode).Sum(x => x.UnitPrice * x.Count) ?? 0, 2);

            var resultAmount1 = Round(_sut.GetItems().Where(x => x.ProductCode == item1.ProductCode).Sum(x => x.Amount) ?? 0, 2);
            Assert.AreEqual(_sut.GetItems().Count(x => x.ProductCode == item1.ProductCode), 1);
            Assert.AreEqual(_sut.GetItems().Where(x => x.ProductCode == item1.ProductCode).Sum(x => x.Count), item1.Count);
            Assert.AreEqual(resultAmount1, expectedAmount);

            var item2 = new Item() { ProductCode = "test2", Count = 1 };
            _sut.Scan(item2);
            expectedAmount = Round(_sut.GetItems().Where(x => x.ProductCode == item2.ProductCode).Sum(x => x.UnitPrice * x.Count) ?? 0,2);
            var resultAmount2 = Round(_sut.GetItems().Where(x => x.ProductCode == item2.ProductCode).Sum(x => x.Amount) ?? 0, 2);
            Assert.AreEqual(_sut.GetItems().Count(x => x.ProductCode == item2.ProductCode), 2);
            Assert.AreEqual(_sut.GetItems().Where(x => x.ProductCode == item2.ProductCode).Sum(x => x.Count), item2.Count + item1.Count);
            Assert.AreEqual(resultAmount2, expectedAmount);
        }

        [Test]
        public void Total_Test()
        {
            var item1 = new Item() { ProductCode = "test2", Count = 3 };
            _sut.Scan(item1);
            var item2 = new Item() { ProductCode = "test1", Count = 2 };
            _sut.Scan(item2);
            var expectedResult = Round(_sut.GetItems().Sum(x => x.Amount) ?? 0, 2);
            var result = Round(_sut.Total(), 2);
            Assert.AreEqual(result, expectedResult);
        }

        [Test]
        public void Scan_With_Incorrect_Item_Test()
        {
            var item = new Item() { ProductCode = "test4", Count = 3 };

            var ex = Assert.Throws<Exception>(() => _sut.Scan(item));
            Assert.AreEqual($"There is no {item.ProductCode} in the Price", ex.Message);
        }
    }
}