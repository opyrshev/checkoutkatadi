﻿using CheckoutKata.Interfaces;
using CheckoutKata.Models;
using CheckoutKata.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace CheckoutKata
{
    class Program
    {
        private static IServiceProvider serviceProvider;
        static void Main(string[] args)
        {
            ConfigureService();

            var checkout = serviceProvider.GetService<ICheckout>();

            try
            {
                var item = new Item() { ProductCode = "Apples", Count = 1 };
                checkout.Scan(item);

                item = new Item() { ProductCode = "Bananas", Count = 1 };
                checkout.Scan(item);

                Bill(checkout.GetItems(), checkout.Total());

                item = new Item() { ProductCode = "Oranges", Count = 3 };
                checkout.Scan(item);

                Bill(checkout.GetItems(), checkout.Total());

                item = new Item() { ProductCode = "Bananas", Count = 1 };
                checkout.Scan(item);

                Bill(checkout.GetItems(), checkout.Total());

                item = new Item() { ProductCode = "Bananas", Count = 1 };
                checkout.Scan(item);

                item = new Item() { ProductCode = "Oranges", Count = 1 };
                checkout.Scan(item);
                Bill(checkout.GetItems(), checkout.Total());

            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            static void Bill(List<Item> items, double total)
            {
                foreach (var element in items)
                {
                    Console.WriteLine($" {element.ProductCode} - {element.UnitPrice} - {element.Count} - {element.Amount}");
                }
                Console.WriteLine($"Total {System.Math.Round(total,2)}");
                Console.WriteLine();
            }
        }

        private static void ConfigureService()
        {
            serviceProvider = new ServiceCollection()
                 .AddSingleton<IPriceService, PriceService>()
                 .AddSingleton<ICheckout, Checkout>()
                 .BuildServiceProvider();
        }
    }
}
