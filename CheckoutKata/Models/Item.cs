﻿namespace CheckoutKata.Models
{
    public class Item
    {
        public string ProductCode { get; set; }

        public double? UnitPrice { get; set; }

        public int Count { get; set; }

        public double? Amount { get; set; }
    }
}
