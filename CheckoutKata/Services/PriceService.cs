﻿using CheckoutKata.Interfaces;
using CheckoutKata.Models;
using System.Collections.Generic;

namespace CheckoutKata.Services
{
    public class PriceService : IPriceService
    {
        private readonly List<Price> Goods = new List<Price>()
            {
                new Price() { ProductCode = "Apples", UnitPrice = 0.50D},
                new Price() { ProductCode = "Bananas", UnitPrice = 0.70D, SpecialOfferCount = 2, SpecialOfferPrice = 1.00D },
                new Price() { ProductCode = "Oranges", UnitPrice = 0.45D, SpecialOfferCount = 3, SpecialOfferPrice = 0.90D },
            };
    public List<Price> GetPrice()
        {
            return Goods;
        }
    }
}
