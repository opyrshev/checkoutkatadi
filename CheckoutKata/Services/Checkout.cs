﻿using CheckoutKata.Models;
using CheckoutKata.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CheckoutKata.Services
{
    
    public class Checkout : ICheckout
    {
        private readonly IPriceService _priceService;
        private List<Item> Items;
        
        public Checkout(IPriceService priceService)
        {
            _priceService = priceService;
            Items = new List<Item>();
        }

        public void Scan(Item item)
        {
            var price = _priceService.GetPrice().Where(x => x.ProductCode == item.ProductCode).FirstOrDefault();

            if (price == null)
            {
                throw new Exception($"There is no {item.ProductCode} in the Price");
            }

            var count = Items.Where(x => x.ProductCode == item.ProductCode).Sum(x => x.Count) + item.Count;
            Items.RemoveAll(x => x.ProductCode == item.ProductCode);

            if (price.SpecialOfferCount != null)
            {
                var usualCount = (count % price.SpecialOfferCount) ?? count;
                AddItem(price.ProductCode, usualCount, usualCount * price.UnitPrice);

                var specialCount = count - usualCount;
                AddItem(price.ProductCode, specialCount, specialCount / price.SpecialOfferCount * price.SpecialOfferPrice ?? 0);
            }
            else
            {
                item.Count = count;
                item.UnitPrice = price.UnitPrice;
                item.Amount = item.UnitPrice * item.Count;
                Items.Add(item);
            }
        }

        public double Total()
        {
            return Items.Sum(x => x.Amount) ?? 0;
        }

        public List<Item> GetItems()
        {
            return Items;
        }

        private void AddItem(string productCode, int count, double amount)
        {
            if (count == 0)
            {
                return;
            }

            var item = new Item()
            {
                ProductCode = productCode,
                Count = count,
                Amount = amount,
                UnitPrice = amount / count
            };

            Items.Add(item);
        }
    }
}
