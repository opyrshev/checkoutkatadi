﻿using CheckoutKata.Models;
using System.Collections.Generic;

namespace CheckoutKata.Interfaces
{
    public interface IPriceService
    {
        List<Price> GetPrice();
    }
}
