﻿using CheckoutKata.Models;
using System.Collections.Generic;

namespace CheckoutKata.Interfaces
{
    public interface ICheckout
    {
        void Scan(Item item);
        double Total();

        List<Item> GetItems();
    }
}
